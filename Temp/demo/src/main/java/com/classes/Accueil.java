

package com.classes;

import java.io.FileInputStream;

import java.io.FileNotFoundException;
import java.io.InputStream;

import javafx.collections.ObservableList;
import javafx.scene.image.*;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
/**
 * Accueil est la classe qui definit l'écran d'accueil
 * Cette classe est caractérisée par les informations suivantes :
 * 
 * Mise en forme du menu d'accueil via une zone de texte et des boutons
 * Accueil permet de lancer le jeu , afficher les règles ainsi que quitter le jeu
 */
public class Accueil extends Stage {
	// les composants de la fenetre
	 /** 
     * VBox qui contient les elements graphiques de la fenêtre
     */
		private VBox 			racine				= new VBox();

		 /** 
     * VBox qui contient les differents label (message et message1)
     */

		private VBox 			zoneTexte				= new VBox();

	 /** 
     * HBox qui contient les boutons (Quitter,Jouer,Regle)
     */
		private HBox 			zoneBoutons 		= new HBox(20);
		 
		 /** 
     * Label utilise pour afficher la première partie du texte
     */
		private Label 			message				= new Label("Vous allez entrer dans une aventure incroyable ");
		
		 /** 
     * Label utilise pour afficher la seconde partie du texte
     */
		private Label 			message1				= new Label("L'aventure d'un cercle qui veut retrouver sa liberté ");
		
		 /** 
     * Bouton qui ferme le logiciel
     */
		private Button 			bnQuitter			= new Button("Quitter");
		
		 /** 
     * Bouton qui lance la partie
     */
		private Button 			bnJouer			= new Button("Jouer");
		 
		 /** 
     * Bouton qui affiche les règles 
     */
		private Button 			bnRegle			= new Button("Regle");
				
		// constructeur : initialisation de la fenetre

 	/** 
     * <b>Constructeur d'Accueil</b>    
     */ 
		public Accueil() {
			/** 
     * Permet d ecrire le titre de la fenetre   
     */
			this.setTitle("Accueil");

			/** 
     * Permet de choisir si la fenetre est redimensionnable (ici false donc pas redimensionnable)
     */
			this.setResizable(false);

			/** 
    		 *  Affichage du contenu créé   
     		*/
			this.setScene(new Scene(creerContenu()));

			/** 
     * La taille de la fenetre est proportionnelle à la taille des elements de la fenetre   
     */
			this.sizeToScene();

		}
		
		// cr�ation du Scene graph

		/** 
     *Methode qui permet de positionner les différents éléments de le fenetre
     *  
     * @return racine qui contient tous les élements de la fenetre
     */ 
		private Parent creerContenu()  {
		
			/** 
     * Permet de définir la largeur du bouton quitter  
     */
			this.bnQuitter.setPrefWidth(120);

				/** 
     * Permet de définir la largeur du bouton jouer 
     */
			this.bnJouer.setPrefWidth(120);

				/** 
     * Permet de définir la largeur du bouton Regle 
     */
			this.bnRegle.setPrefWidth(120);
		
					/** 
     * Regroupe les messages dans la même zone de message
     */
			zoneTexte.getChildren().addAll(message,message1);

							/** 
     * Regroupe les boutons dans la même zone de bouton
     */
			zoneBoutons.getChildren().addAll(bnQuitter,bnJouer,bnRegle);

							/** 
     * Aligne les boutons au centre de la zone de bouton
     */
			zoneBoutons.setAlignment(Pos.CENTER);


									/** 
     * Regroupe les différentes zones dans la racine 
     */
			racine.getChildren().addAll(zoneTexte, zoneBoutons);

			/** 
     * Permet de faire une marge autour de la zone de texte 
     */
			VBox.setMargin(zoneTexte, new Insets(20, 20, 20, 20));

			/** 
     * Permet de faire une marge autour de la zone de bouton
     */
			VBox.setMargin(zoneBoutons, new Insets(20, 20, 20, 20));

/** 
     * Appuyer sur le bouton quitter execute la methode fermer
     */
			bnQuitter.setOnAction(e -> fermer());

			/** 
     * Appuyer sur le bouton jouer execute la methode jouer
     */
			bnJouer.setOnAction(e -> jouer());

			/** 
     * Appuyer sur le bouton regle execute la methode regle
     */
			bnRegle.setOnAction(e -> regle());
			return racine;
		}

	/** 
     * Ferme la fenetre
     */
		public void fermer(){
			
		 System.exit(0);
		}

		/** 
     * ouvre la fenetre du jeu et enleve la pause
     */

		public void jouer(){

				MainApp.ouvrirFenetreJouer();
				Test.setGameStarted(true);		
				
		}

		/** 	
     * Ouvre la fenetre des regles
     */
		public void regle(){
			MainApp.ouvrirFenetreMenu();
		}
}
