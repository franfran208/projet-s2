


package com.classes;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.stage.Stage;


/**
 * MainApp est la classe qui permet d'ouvrir les fenetres et de lire un fichier texte pour le transformer en map
 * Cette classe est caractérisée par les informations suivantes :
 * 
 * Scan d'un fichier texte pour creer une map
 * Gestion des fenetres
 */
public class MainApp extends Application {
    static private Menu fmenu;
	static private Test fTest;
    static private Accueil fAccueil;
    private static String ligne;
    private char[] tableau;
    private ArrayList<String> map;
    private static Group racine = new Group();
    private static ArrayList<Mur> tab = new ArrayList<Mur>();
    private int nbCol=40;

    
   /** 
     * <b>Constructeur de MainApp</b>    
     */ 

    public MainApp() {      
   /** 
     * Creation du liste de caractere   
     */ 
        this.map = new ArrayList<String>();
    }

/** 
     * methode qui scan le fichier texte et initialise les fenetres
     *@throws FileNotFoundException
     * Propagation de l'exception si le chemin ne permet pas de trouver un fichier
     */
    public void start(Stage primaryStage) throws InterruptedException, FileNotFoundException{
        FileInputStream file = new FileInputStream("C:/Users/franc/OneDrive/Documents/Projet S2/projet-s2/projet-s2/Temp/demo/src/main/java/com/classes/map1.txt");   
        Scanner scanner = new Scanner(file);
        
        
        while(scanner.hasNextLine()){
          
            ligne=scanner.nextLine();
            tableau = ligne.toCharArray();
            for(int i=0;i<tableau.length;i++){
          
            map.add(Character.toString(tableau[i]));
          
            }
        
        } 
        scanner.close(); 
        racine = constructionMap(map);
        

        fTest = new Test();
        fmenu = new Menu();
        fAccueil = new Accueil();

       
        primaryStage = fAccueil;
        primaryStage.show();
        
    }

  
   /** 
     * methode qui affiche la fenetre application au lancement de la classe MainApp  
     */ 
    public static void main(String[] args) {
       
        Application.launch(); 
        
        

    }

/** 
     * methode qui ouvre la fenetre menu  
     */ 
    static public void ouvrirFenetreMenu(){
		
		fmenu.show();

	}

  /** 
     * methode qui ouvre la fenetre de jeu et qui ferme la fenetre Accueil
     */
    static public void ouvrirFenetreJouer(){
		
		fTest.show();
        fAccueil.close();
	}

  /** 
     * methode qui ouvre la fenetre Accueil
     */
    static public void ouvrirFenetreAccueil(){

		fAccueil.show();
        
	}

/** 
     * methode qui cherche les 'M' dans le fichier texte, pour creer un mur qui partage les memes coordonnées
     */
    public Group constructionMap(ArrayList<String> map){
        int x = 0;
        int y = 0;
        for(int i = 0; i<map.size();i++){
          
          
          if(x>=nbCol){
           y++;
           x=0;
    
          }
          if(map.get(i).equals("M")){
            
            
            tab.add(new Mur(false,x*20,y*20));
            
          }
          x++; 
        }
       // for (int p = 0;p<tab.size();p++){
         // System.out.println(p + "= "+ tab.get(p).getX());
          //System.out.println(p + "= "+ tab.get(p).getY());
       // }
        return Mur.dessinMur(tab);
      }

    public static Group getRacine() {
        return racine;
    }

    public static void setRacine(Group racine) {
        MainApp.racine = racine;
    }

    
      
}
