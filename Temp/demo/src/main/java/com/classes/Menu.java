
package com.classes;

import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Menu est la classe qui définit l'écran des regles
 * Cette classe est caractérisée par les informations suivantes :
 * 
 * Mise en forme du menu des regles via une zone de texte et un bouton
 * Le bouton Quitter ferme la fenetre
 */

public class Menu extends Stage {
	
	// les composants de la fenetre
	 /** 
     * VBox qui contient les elements graphiques de la fenêtre
     */

		private VBox 			racine				= new VBox();

		 /** 
     * VBox qui contient les differents label (message et message1)
     */

		private VBox 			zoneTexte 		= new VBox(20);

		/** 
     * HBox qui contient les boutons (Quitter,Jouer,Regle)
     */

		private HBox 			zoneBoutons 		= new HBox(20);
		

		private Label 			message				= new Label("Règles du jeu: ");
		private Label 			message3				= new Label("");
		private Label 			message1				= new Label("Utiliser les flèches du clavier pour bouger le personnage ");
		private Label 			message2				= new Label("Utiliser la touche M pour accéder au menu de règles depuis le jeu ");
		
		private Label 			message4				= new Label("Utiliser la touche A pour accéder à l'accueil depuis le jeu");
		private Label 			message5				= new Label("But du jeu: ");
		private Button 			bnQuitter			= new Button("Quitter");
				
		// constructeur : initialisation de la fenetre
		/** 
     * <b>Constructeur de Menu</b>    
     */ 

		public Menu(){
					/** 
     * Permet d ecrire le titre de la fenetre   
     */
			this.setTitle("Menu");

			/** 
     * Permet de choisir si la fenetre est redimensionnable (ici false donc pas redimensionnable)
     */
			this.setResizable(false);

			/** 
    		 *  Affichage du contenu créé   
     		*/
			this.setScene(new Scene(creerContenu()));

			/** 
     * La taille de la fenetre est proportionnelle à la taille des elements de la fenetre   
     */

			this.sizeToScene();
		}
		
		// cr�ation du Scene graph

		/** 
     *Methode qui permet de positionner les différents éléments de le fenetre
     *  
     * @return racine qui contient tous les élements de la fenetre
     */ 

		private Parent creerContenu() {
		
		/** 
     * Permet de définir la largeur du bouton quitter  
     */

			this.bnQuitter.setPrefWidth(100);
			
			/** 
     * Regroupe les messages dans la même zone de message
     */

			zoneTexte.getChildren().addAll(message,message3,message1,message2,message4,message5);
						
		

			zoneBoutons.getChildren().add(bnQuitter);
			zoneBoutons.setAlignment(Pos.CENTER);

			
			/** 
     		* Regroupe les différentes zones dans la racine 
     		*/
			racine.getChildren().addAll(zoneTexte, zoneBoutons);
			/** 
     		* Permet de faire une marge autour de la zone de texte 
     		*/
			VBox.setMargin(zoneTexte, new Insets(20, 20, 20, 20));

			/** 
     		* Permet de faire une marge autour de la zone de bouton
     		*/
			VBox.setMargin(zoneBoutons, new Insets(20, 20, 20, 20));

	/** 
     * Appuyer sur le bouton quitter execute la methode fermer
     */

			bnQuitter.setOnAction(e -> fermer());
			return racine;
		}

		/** 
     * Ferme la fenetre
     */

		public void fermer(){
			this.close();
			
		}

}
