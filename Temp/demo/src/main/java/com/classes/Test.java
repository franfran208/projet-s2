package com.classes;


import java.util.ArrayList;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

import javafx.scene.control.Label;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import javafx.scene.layout.StackPane;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * Test est la classe qui definit la fenetre de jeu le ou les ennemis, affiche le joueur etc...
 * Cette classe est caracterisee par les informations suivantes :
 * <ul>
 * <li>KeyCode tQuit</li>
 * <li>KeyCode tA</li>
 *  <li>static Label message</li>
 *  <li>int perso_x</li>
 *  <li>int perso_y</li>
 *  <li>int ennemi_x</li>
 *  <li>int rayon_perso</li>
 *  <li>int taille_ennemi</li>
 *  <li>static boolean gameStarted</li>
 * </ul>
 * Faire apparaitre la fenetre de jeu, le joueur, le ou les ennmis, les murs , la map
 * </p>
 *  Label message
 *  boolean gameStarted
 * </p>
 * @author The Undying's quest team
 */ 	  
public class Test extends Stage {
    /** 
     * defini la touche a appuyer pour quitter le jeu 
     */
    private KeyCode tQuit = KeyCode.ESCAPE;
 
    /** 
     * defini la touche a appuyer pour afficher l'accueil du jeu 
     */
    private KeyCode tA = KeyCode.A;
    
    /** 
     * sert a ecrire sur l espace de jeu pour faire des tests 
     */
	private static Label 		message			= new Label();
    /** 
     * defini la coordonnee x du personnage
     */
    private int perso_x = 500;
    /** 
     * defini la coordonnee y du personnage
     */
    private int perso_y = 500;
    /** 
     * defini la coordonnee x de l'ennemi
     */
    private int ennemi_x = 100;
    /** 
     * defini la coordonnee y de l'ennemi
     */
    private int ennemi_y = 100;
    /** 
     * defini le diamètre du personnage
     */
    private int rayon_perso = 50;
    /** 
     * defini la longueur et la largeur de l'ennemi
     */
    private int taille_ennemi = 50;
    /** 
     * quand vrai lance le jeu et les anmiations
     */
    private static boolean gameStarted;
    /** 
     * racine a ajouter au stackpane
     */
    private Group racine = new Group();
    
    

    
    /** 
     * defini l'etape dans l'animation de l'ennemi
     */
    private int i = 1;
    /** 
     * defini la racine du jeu que l'on affiche apres avoir associer aux elements
     */
    private StackPane sp =new StackPane();
    /** 
     * espace de dessin (perso, ennemi, murs)
     */
    private Canvas canvas = new Canvas(800, 800);
    /** 
     * defini le personnage que l'on affiche
     */
    private Circle perso = new Circle(perso_x,perso_y,rayon_perso);
    /** 
     * defini l'ennemi que l'on affiche'
     */
    private Rectangle ennemi = new Rectangle(ennemi_x ,ennemi_y ,taille_ennemi,taille_ennemi);
    /** 
     * ArrayList de Mur qui stocke les coordonnes des mur de la carte
     */
    private static ArrayList<Mur> tab = new ArrayList<Mur>();


    
	
	 /** 
     * <b>Constructeur de Test</b> 
     * 
     * Permet de construire la fenetre de jeu
     * 
     */ 
	public Test() throws InterruptedException{ 
        this.setTitle("Quest");
        
        GraphicsContext gc = canvas.getGraphicsContext2D();
        Scene laScene=new Scene(sp);
		Timeline tl = new Timeline(new KeyFrame(Duration.millis(75), e -> run(gc)));
        racine = MainApp.getRacine();
        

        tl.setCycleCount(Timeline.INDEFINITE);
       

        
      
        sp.getChildren().addAll(canvas,message,racine);
        this.setScene(laScene);
		this.show();
		tl.play();
        laScene.setOnKeyPressed(e-> gererTouche(e));
       

        
       
    
	}
    /** 
     * permet d'avoir la valeur de gameStarted  depuis une autre classe
     *  
     * @return true ou false 
     * 
     *   
     */ 
    public boolean isGameStarted() {
        return gameStarted;
    }

    /** 
     * permet de definir la  la valeur de gameStarted depuis une autre classe 
     *  
     * @param gameDtarted
     *      jeu en marche ou non boolean 
     */
    public static void setGameStarted(boolean gameStarted) {
        Test.gameStarted = gameStarted;
    }
    /** 
     * Permet de calculer les collisions du personnage avec les ennemis 
     * @param gc  
     *     permet d'ecrire sur l'espace de jeu pour les tests 
     * 
     */ 
    public void collision(GraphicsContext gc){
        
        
        
        if(perso_x+rayon_perso/2>=ennemi_x-taille_ennemi && perso_y-rayon_perso/2<=ennemi_y+taille_ennemi){
            
            gc.setFill(Color.RED);
            gc.fillText("a",100,50);
            
        }
        else if((perso_x+rayon_perso/2>=ennemi_x-taille_ennemi || perso_x-rayon_perso/2<=ennemi_x+taille_ennemi)&& perso_y-rayon_perso/2<=ennemi_y+taille_ennemi){
            
            gc.setFill(Color.RED);
            gc.fillText("b",100,50);
        }
        else if(perso_x+rayon_perso/2>=ennemi_x-taille_ennemi && (perso_y+rayon_perso/2>=ennemi_y-taille_ennemi || perso_y-rayon_perso/2<=ennemi_x+taille_ennemi)){
            
            gc.setFill(Color.RED);
            gc.fillText("c",100,50); 
        }
        else if((perso_x+rayon_perso/2>=ennemi_x-taille_ennemi || perso_x-rayon_perso/2<=ennemi_x+taille_ennemi)&& perso_y+rayon_perso/2>=ennemi_y-taille_ennemi){
            
            gc.setFill(Color.RED);
            gc.fillText("d",100,50);
        }
       
        else{
            gc.setFill(Color.RED);
            gc.fillText("bite",100,50);
            
        }

        
    }

  /** 
     * Permet de mettre a jour les elements a afficher a l'ecran 
     * @param gc 
     *     espace ou l'on affiche et rempli les elements 
     * 
     */ 
	public void run(GraphicsContext gc){ 
        gc.setFill(Color.BLACK);
        gc.fillRect(0,0, 800,800 );
        gc.setFill(Color.RED);
        
       
        
        
        gc.fillRect(ennemi_x,ennemi_y , taille_ennemi,taille_ennemi );
        
        
        if(gameStarted){
            
            if(i<=20){
                collision(gc);
                ennemi_x= ennemi_x+10;
                ennemi.setLayoutX(ennemi_x);
                
                gc.fillRect(ennemi_x,ennemi_y , taille_ennemi,taille_ennemi );
                i++;
            }
            else if(i>20 && i<=40){
                collision(gc);
                
                ennemi_y=ennemi_y+10;
                ennemi.setLayoutX(ennemi_x);
                
                gc.fillRect(ennemi_x,ennemi_y , taille_ennemi,taille_ennemi );
                i++;

            }
            else if(i>40 && i<=60){
                collision(gc);
                ennemi_x=ennemi_x-10;
                ennemi.setLayoutY(ennemi_y);
                
                gc.fillRect(ennemi_x,ennemi_y , taille_ennemi,taille_ennemi );
                i++;
            }
            else if(i>60 && i<=80){
                collision(gc);
                ennemi_y=ennemi_y-10;
                ennemi.setLayoutY(ennemi_y);
                
                gc.fillRect(ennemi_x,ennemi_y , taille_ennemi,taille_ennemi );
                i++;
            }
            if(i>80){
                collision(gc);
                gc.fillRect(ennemi_x,ennemi_y , taille_ennemi,taille_ennemi );
                i=0;
            }
            
            
        }
        Mur.updateMur(tab, gc);
        gc.setFill(Color.BLUE);
        gc.fillOval(perso_x, perso_y, rayon_perso, rayon_perso);
        
        collision(gc);
       
       
       
       
        
    }
    /** 
     * Permet de gerer les touches du clavier et de les mettre en lien avec des actions 
     * @param e
     *     touche pressee
     *  
     */ 
    private void gererTouche(KeyEvent e) {
        if(e.getCode().equals(KeyCode.LEFT)){
			
            perso_x-=10;
		}
        if(e.getCode().equals(KeyCode.RIGHT)){
			
			perso_x+=10;
		}
        if(e.getCode().equals(KeyCode.DOWN)){
			
			perso_y+=10;
 
		}
        if(e.getCode().equals(KeyCode.UP)){
			
			perso_y-=10;
            
		}

        if (tQuit.equals(e.getCode())){
            System.exit(0);
        }

        
        if(tA.equals(e.getCode())){
            gameStarted=false;
            
            MainApp.ouvrirFenetreAccueil();
         }
         if(perso.intersects(ennemi.getBoundsInLocal())) {
            System.exit(0);
        }


       
       
            
        
    }

	
	
}
