package com.classes;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import javafx.collections.ObservableList;
import javafx.scene.image.*;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Accueil extends Stage {
	// les composants de la fen�tre
		private VBox 			racine				= new VBox();
		private VBox 			zoneTexte				= new VBox();
	
		private HBox 			zoneBoutons 		= new HBox(20);
		
		private Label 			message				= new Label("Vous allez entrer dans une aventure incroyable ");
		private Label 			message1				= new Label("L'aventure d'un cercle qui veut retrouver sa liberté ");
		private Button 			bnAnnuler			= new Button("Quitter");
		private Button 			bnJouer			= new Button("Jouer");
		private Button 			bnRegle			= new Button("Regle");
				
		// constructeur : initialisation de la fen�tre
		public Accueil() {
			this.setTitle("Accueil");
			this.setResizable(false);
			this.setScene(new Scene(creerContenu()));
			this.sizeToScene();

		}
		
		// cr�ation du Scene graph
		private Parent creerContenu()  {
		
			this.bnAnnuler.setPrefWidth(120);
			this.bnJouer.setPrefWidth(120);
			this.bnRegle.setPrefWidth(120);
		
			zoneTexte.getChildren().addAll(message,message1);
			zoneBoutons.getChildren().addAll(bnAnnuler,bnJouer,bnRegle);
			zoneBoutons.setAlignment(Pos.CENTER);
			
			racine.getChildren().addAll(zoneTexte, zoneBoutons);
			VBox.setMargin(zoneTexte, new Insets(20, 20, 20, 20));
			VBox.setMargin(zoneBoutons, new Insets(20, 20, 20, 20));

			bnAnnuler.setOnAction(e -> fermer());
			bnJouer.setOnAction(e -> jouer());
			bnRegle.setOnAction(e -> regle());
			return racine;
		}
		public void fermer(){
			
		 System.exit(0);
		}

		public void jouer(){

				MainApp.ouvrirFenetreJouer();
				Test.setGameStarted(true);
				
				
		}

		public void regle(){
			MainApp.ouvrirFenetreMenu();
		}
}
