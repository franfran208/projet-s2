package com.classes;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.stage.Stage;



public class MainApp extends Application {
    static private Menu fmenu;
	static private Test fTest;
    static private Accueil fAccueil;
    private static String ligne;
    private char[] tableau;
    private ArrayList<String> map;
    private static Group racine = new Group();
    private ArrayList<Mur> tab = new ArrayList<Mur>();
    private int nbCol=40;

    
   
    public MainApp() {
        this.map = new ArrayList<String>();
    }

    public void start(Stage primaryStage) throws InterruptedException, FileNotFoundException{
        FileInputStream file = new FileInputStream("https://gitlab.com/franfran208/projet-s2/-/blob/master/projet/src/main/java/code/projet/map1.txt");   
        Scanner scanner = new Scanner(file);
        
        
        while(scanner.hasNextLine()){
          
            ligne=scanner.nextLine();
            tableau = ligne.toCharArray();
            for(int i=0;i<tableau.length;i++){
          
            map.add(Character.toString(tableau[i]));
          
            }
        
        } 
        scanner.close(); 
        racine = constructionMap(map);
        

        fTest = new Test();
        fmenu = new Menu();
        fAccueil = new Accueil();

       
        primaryStage = fAccueil;
        primaryStage.show();
    }

    public static void main(String[] args) {
       
        Application.launch(); 
        
        

    }

    static public void ouvrirFenetreMenu(){
		
		fmenu.show();

	}
    static public void ouvrirFenetreJouer(){
		
		fTest.show();
        fAccueil.close();
	}
    static public void ouvrirFenetreAccueil(){

		fAccueil.show();
        
	}


    public Group constructionMap(ArrayList<String> map){
        int x = 0;
        int y = 0;
        for(int i = 0; i<map.size();i++){
          
          
          if(x>=nbCol){
           y++;
           x=0;
    
          }
          if(map.get(i).equals("M")){
            
            
            tab.add(new Mur(false,x*20,y*20));
            
          }
          x++; 
        }
        for (int p = 0;p<tab.size();p++){
          System.out.println(p + "= "+ tab.get(p).getX());
          System.out.println(p + "= "+ tab.get(p).getY());
        }
        return Mur.dessinMur(tab);
      }

    public static Group getRacine() {
        return racine;
    }

    public static void setRacine(Group racine) {
        MainApp.racine = racine;
    }

    
      
}
