package com.classes;

import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Menu extends Stage {
	// les composants de la fen�tre
		private VBox 			racine				= new VBox();
		private VBox 			zoneTexte 		= new VBox(20);
		private HBox 			zoneBoutons 		= new HBox(20);
		
		private Label 			message				= new Label("Règles du jeu: ");
		private Label 			message3				= new Label("");
		private Label 			message1				= new Label("Utiliser les flèches du clavier pour bouger le personnage ");
		private Label 			message2				= new Label("Utiliser la touche M pour accéder au menu de règles depuis le jeu ");
		
		private Label 			message4				= new Label("Utiliser la touche A pour accéder à l'accueil depuis le jeu");
		private Label 			message5				= new Label("But du jeu: ");
		private Button 			bnAnnuler			= new Button("Quitter");
				
		// constructeur : initialisation de la fen�tre
		public Menu(){
			this.setTitle("Menu");
			this.setResizable(false);
			this.setScene(new Scene(creerContenu()));
			this.sizeToScene();
		}
		
		// cr�ation du Scene graph
		private Parent creerContenu() {
		
			this.bnAnnuler.setPrefWidth(100);
			
			zoneTexte.getChildren().addAll(message,message3,message1,message2,message4,message5);
						
			zoneBoutons.getChildren().add(bnAnnuler);
			zoneBoutons.setAlignment(Pos.CENTER);
			
			racine.getChildren().addAll(zoneTexte, zoneBoutons);
			VBox.setMargin(zoneTexte, new Insets(20, 20, 20, 20));
			VBox.setMargin(zoneBoutons, new Insets(20, 20, 20, 20));

			bnAnnuler.setOnAction(e -> fermer());
			return racine;
		}
		public void fermer(){
			this.close();
			
		}

}
