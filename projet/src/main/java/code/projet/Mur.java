package com.classes;



import java.util.ArrayList;

import javafx.scene.Group;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
/**
 * Mur est la classe qui definit un rectangle de collision avec le joueur positionee à l'aide d'un fichier texte
 * Cette classe est caracterisee par les informations suivantes :
 * <ul>
 * <li>boolean traersable</li>
 * <li>Rectangle mur</li>
 *  <li>int x</li>
 *  <li>int y</li>
 * </ul>
 * Faire apparaitre les murs aux coordonnees indiquées sur le fichier texte
 * </p>
 * pas de static
 * </p>
 * @author The Undying's quest team
 */
public class Mur {
     /** 
     * si false le Mur n'est pas traversable par le joueur 
     */
    private boolean traversable ;
     /** 
     * permet de dessiner le Mur sur la fenetre de jeu 
     */
    private Rectangle mur;
     /** 
     * coordonnees x du Mur vas servir a construire le Rectangle mur et a recuperer la coordonnee x du mur
     */
    private int x;
    /** 
     * coordonnees y du Mur vas servir a construire le Rectangle mur et a recuperer la coordonnee y du mur
     */
    private int y;

     /** 
     * <b>Constructeur de Mur</b> 
     * 
     *  la variable mur est construite avec les variables x et y
     * 
     * 
     * @param traversable 
     *     definit l'etat du mur possible de le traverser ou non 
     * @param x 
     *     coordonnee x du Mur
     * @param y
     *     coordonnee y du Mur
     */ 
    public Mur(boolean traversable, int x, int y) {
        this.traversable = traversable;
        this.x = x;
        this.y = y;
        this.mur = new Rectangle(x,y,20,20);
    }

    /** 
     * permet d'avoir la valeur de traversable d'un mur depuis une autre classe
     *  
     * @return true ou false 
     * 
     *   
     */ 
    public boolean isTraversable() {
        return traversable;
    }

    /** 
     * permet de definir la  la valeur de traversable depuis une autre classe 
     *  
     * @param traversable
     *      etat boolean 
     */
    public void setTraversable(boolean traversable) {
        this.traversable = traversable;
    }

    /** 
     * permet d'avoir la valeur la coordonnee x d'un mur depuis une autre classe
     *  
     * @return un int
     * 
     *   
     */ 
    public int getX() {
        return x;
    }

    /** 
     * permet de definir la  la valeur de la coordonne x d'un mur depuis une autre classe 
     *  
     * @param x
     *      coordonnee x int 
     *   
     */
    public void setX(int x) {
        this.x = x;
    }

    /** 
     * permet d'avoir la valeur la coordonnee y d'un mur depuis une autre classe
     *  
     * @return un int
     * 
     *   
     */ 
    public int getY() {
        return y;
    }

    /** 
     * permet de definir la  la valeur de la coordonne x d'un mur depuis une autre classe 
     *  
     * @param x
     *      coordonnee x int 
     *   
     */
    public void setY(int y) {
        this.y = y;
    }
    /** 
     * permet de retourner un groupe de mur dessines a partir d'une ArrayList de Mur 
     * @param m
     *     ArrayList de Mur 
     * @return retourne un groupe g de Mur dessines qu'on ajoute a sa racine pour les afficher 
     * 
     */ 
    public static Group dessinMur(ArrayList<Mur> m){
        Group g = new Group();
        for(int i=0; i< m.size();i++){
            
            m.get(i).mur.setFill(Color.WHITE);
            g.getChildren().add(m.get(i).mur);
        }
        return g;

    }

    /** 
     * permet d'actualiser une arrayList de mur deja raccorder a la racine et au canvas 
     * @param m
     *     ArrayList de Mur 
     * @param gc
     *     espace ou l'on dessine les elements 
     * r 
     * 
     */ 
    public static void updateMur(ArrayList<Mur> m, GraphicsContext gc){
        for(int i=0; i< m.size();i++){
            Rectangle r = m.get(i).mur;
            gc.setFill(Color.WHITE);
            gc.fillRect(r.getX(), r.getY(), 20, 20);
        
        }
    }

    

    
    
}
